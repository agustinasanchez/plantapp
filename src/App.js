import React from 'react';
import Home from './components/Home';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import { green, lightGreen } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary:{
      light: '#2e7031',
      main: '#43a047',
      dark: '#68b36b',
      contrastText: '#fff',
    },
    secondary: lightGreen, 
  },
})

export default function PlantasApp() {
  return <ThemeProvider theme={theme}>
    <Home />
  </ThemeProvider>
}



import { subtipos } from './subtipos'

const plantasDeInterior = subtipos.find((subtipo) => subtipo.id === 1)
const cactus = subtipos.find((subtipo) => subtipo.id === 2)
const suculentas = subtipos.find((subtipo) => subtipo.id === 4)

export const plantas = [{
    titulo:"Opuntia",
    subtipo: cactus,
    cardImage:"cactus1.jpg",
    descripcion:"Opuntia o nopal es un género de plantas de la familia de las cactáceas que consta de más de 300 especies, todas nativas del continente americano, que habitan desde el sur de Estados Unidos hasta la Patagonia.",
    detalles:"Frecuencia de riego: regar cuando el sustrato esté seco. Luz: Necesita buena luz natural."
  },
  {
    titulo:"Fitonia",
    subtipo: plantasDeInterior,
    cardImage:"fitonia.jpg",
    descripcion:"Fittonia es un género con dos especies perteneciente a la familia Acanthaceae. ​ Es nativo de las selvas tropicales de Sudamérica. Es una planta para mantener en interior y con bastante humedad.",
    detalles:"Frecuencia de riego: regar cuando el sustrato esté seco. Luz: Necesita buena luz natural."
  },
  {
    titulo:"Monstera",
    subtipo: plantasDeInterior,
    cardImage:"monstera.jpg",
    descripcion:"Monstera deliciosa, llamada comúnmente cerimán o costilla de Adán, es una especie de trepadora, endémica de selvas tropicales, que se distribuye desde el centro y sur de México hasta el norte de Argentina.",
    detalles:"Frecuencia de riego: regar cuando el sustrato esté seco. Luz: Necesita buena luz natural."
  },
  {
    titulo:"Palo de Agua",
    subtipo: plantasDeInterior,
    cardImage:"palodeagua.jpg",
    descripcion:"Epipremnum aureum, comúnmente conocido como potus, pothos o potos es una especie de la familia Araceae nativa del sudeste asiático y Nueva Guinea. En ocasiones es confundida con philodendron en las floristerías.",
    detalles:"Frecuencia de riego: regar cuando el sustrato esté seco. Luz: Necesita buena luz natural."
  },
  {
    titulo:"Sanseviera",
    subtipo: plantasDeInterior,
    cardImage:"sansevieria.jpg",
    descripcion:"Epipremnum aureum, comúnmente conocido como potus, pothos o potos es una especie de la familia Araceae nativa del sudeste asiático y Nueva Guinea. En ocasiones es confundida con philodendron en las floristerías.",
    detalles:"Frecuencia de riego: regar cuando el sustrato esté seco. Luz: Necesita buena luz natural."
  },
  {
    titulo:"San Pedro",
    subtipo: cactus,
    cardImage:"sanpedro.jpg",
    descripcion:"Epipremnum aureum, comúnmente conocido como potus, pothos o potos es una especie de la familia Araceae nativa del sudeste asiático y Nueva Guinea. En ocasiones es confundida con philodendron en las floristerías.",
    detalles:"Frecuencia de riego: regar cuando el sustrato esté seco. Luz: Necesita buena luz natural."
  },
  {
    titulo:"Mammillaria",
    subtipo: cactus,
    cardImage:"mammillaria.jpg",
    descripcion:"Epipremnum aureum, comúnmente conocido como potus, pothos o potos es una especie de la familia Araceae nativa del sudeste asiático y Nueva Guinea. En ocasiones es confundida con philodendron en las floristerías.",
    detalles:"Frecuencia de riego: regar cuando el sustrato esté seco. Luz: Necesita buena luz natural."
  },
  {
    titulo:"Echeveria",
    subtipo: suculentas,
    cardImage:"echeveria.jpg",
    descripcion:"Echeveria elegans es una especie de planta suculenta perteneciente a la familia de las crasuláceas. Emerge de rosetas sin tallo, formando densas alfombras con los estolones; alcanza hasta 10 cm de diámetro.",
    detalles:"Frecuencia de riego: Moderado. No necesita tanta agua ya que pueden pudrirse las raices. Luz: Necesita buena luz natural pero no sol directo."
  },
]
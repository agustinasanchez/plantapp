import { green, lime, lightGreen } from '@material-ui/core/colors';

export const subtipos = [
    {
        id:1,
        nombre: 'Plantas de Interior',
        color: lightGreen[500],
        },
    {
        id:2,
        nombre: 'Cactus',
        color: lime[500],
    },
    {
        id:3,
        nombre: 'Plantas de Exterior',
        color: green[800],
    },
    {
        id:4,
        nombre: 'Suculentas',
        color: green[200],
    }
]
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { plantas } from '../data/plantas';
import { subtipos } from '../data/subtipos';
import { Grid, Chip, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CardDeAgus from './CardDeAgus';
import NavBar from './NavBar';
import Header from './Header';
import FiltroChip from './Filtros';
import AgregarPlantaModal from './Agregar';

const useStyles = makeStyles((theme) =>({
  botonModal:{
    position:'fixed',
    right:theme.spacing(2),
    bottom:theme.spacing(2)
  }
}))


export default function Home() {
  const classes = useStyles()
  const [busqueda, setBusqueda] = useState('');
  const [open, setOpen] = React.useState(false);
  const [subtiposFiltro, setSubtiposFiltro] = useState(subtipos.map(subtipo => {
    const nuevo = { ...subtipo }
    nuevo.activado = false
    return nuevo
  }));

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
      setOpen(false);
  };

  function agregarPlanta(planta){
    plantas.push(planta)
  }

  function obtenerPlantas() {
    let nuevasPlantas = plantas.map(planta => {
      return {
        ...planta,
        avatar: planta.subtipo.nombre.charAt(0),
      }
    })
    if (busqueda !== '') {
      nuevasPlantas = nuevasPlantas.filter((planta) => planta.titulo.toLowerCase().includes(busqueda.toLowerCase()) || planta.subtipo.nombre.toLowerCase().includes(busqueda.toLowerCase())) 
    }
    if (subtiposFiltro.some(subtipo => subtipo.activado)) {
      nuevasPlantas = nuevasPlantas.filter((planta) => subtiposFiltro.find(subtipo => subtipo.id === planta.subtipo.id).activado)
    }
    return nuevasPlantas
  }

  return (
    <>
      <NavBar buscar={setBusqueda} valor={busqueda} titulo='Mis plantas' />
      <Header />
      <AgregarPlantaModal estaAbierto={open} handleClose={handleClose} agregarPlanta={agregarPlanta}/>
      <FiltroChip subtipos={subtiposFiltro} setSubtipos={setSubtiposFiltro} />
      <Grid container>
        {obtenerPlantas().length === 0 ?
          busqueda === "pablo" ?
            "No se encontró ninguna planta pero Pablo es un pelotudo" :
            "No se encontró ninguna planta" : 
          obtenerPlantas().map((planta) => {
          return <Grid item md={3} sm={6} xs={12}>
            <CardDeAgus titulo={planta.titulo} subtipo={planta.subtipo.nombre} cardImage={planta.cardImage}
             descripcion={planta.descripcion} avatar={planta.avatar} avatarColor={planta.subtipo.color} detalles={planta.detalles} />
          </Grid>
        }) 
        }
      </Grid>
        <Fab className={classes.botonModal} onClick={handleOpen} color="primary" aria-label="add">
        <AddIcon />
      </Fab>
    </>
  );
}



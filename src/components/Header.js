import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
});

export default function Header() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <img className={classes.image}/>
            <Typography variant="h2" component="h2" gutterBottom>
                
          </Typography>
        </div>
    )
}
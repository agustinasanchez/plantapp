import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Chip } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));


export default function FiltroChip(props) {
  const classes = useStyles()

  function chipClick(subtipo) {
    subtipo.activado = !subtipo.activado
    props.setSubtipos([...props.subtipos])
  }
  return <div className={classes.root}>
    {props.subtipos.map(subtipo => <Chip color={subtipo.activado ? "primary" : 'default'} label={subtipo.nombre} onClick={() => chipClick(subtipo)} />)}

  </div>
}
import React, { useState } from 'react';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowDownUpIcon from '@material-ui/icons/ArrowUpward';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 300,
      borderRadius: 32,
      margin: 20,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    avatar: {
      backgroundColor: green[500],
    },
    button: {
      marginTop: theme.spacing(2),
    }
  }));

export default function CardDeAgus(props){
    const classes = useStyles();
    const [detallesDesplegados, setDetallesDesplegados] = useState(false)

    function DesplegarDetalles() {
      setDetallesDesplegados(!detallesDesplegados)
    }

    return <Card className={classes.root}>
    <CardHeader
      avatar={
        <Avatar aria-label="recipe" style={{backgroundColor:props.avatarColor}} >
          {props.avatar}
        </Avatar>
      }
      action={
        <IconButton aria-label="settings">
          
        </IconButton>
      }
      title={props.titulo}
      subheader={props.subtipo}
    />
    <CardMedia
      className={classes.media}
      image={props.cardImage}
      title={`${props.titulo} ${props.subtipo}`}
    />
    <CardContent>
      <Typography variant="body2" color="textSecondary" component="p">
        {props.descripcion}
      </Typography>
      <Typography variant="body2" color="textSecondary">
      {detallesDesplegados && props.detalles}
      </Typography>

      <Button onClick={() => DesplegarDetalles()}
        variant="contained"
        color={detallesDesplegados ? "default" : "primary"}
        size="small"
        className={classes.button}
        endIcon={detallesDesplegados ? <ArrowDownUpIcon/> : <ArrowDownwardIcon/>}
      >
        Detalles
      </Button>
    </CardContent>
    <CardActions disableSpacing>
      <IconButton aria-label="add to favorites">
        
      </IconButton>
      <IconButton aria-label="share">
      
      </IconButton>
    </CardActions>
  </Card>
  }
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { subtipos } from '../data/subtipos';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';


function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles((theme) => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(4),
    },
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(3,0,0)
        },
    },
    button: {
        marginTop: theme.spacing(4)
    }
}));

export default function AgregarPlantaModal(props) {
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);
    const [nombre, setNombre] = React.useState('');
    const [descripcion, setDescripcion] = React.useState('');
    const [detalles, setDetalles] = React.useState('');
    const [subtipoId, setSubtipo] = React.useState('');
    const [imagen, setImagen] = React.useState('');
    const [petFriendly, setPetFriendly] = React.useState(false);

    function guardar(){
        props.agregarPlanta({
            titulo:nombre,
            subtipo: subtipos.find((subtipo) => subtipo.id === subtipoId),
            cardImage:imagen,
            descripcion:descripcion,
            detalles:detalles
          })
        props.handleClose()
    }

    const body = (
        <div style={modalStyle} className={classes.paper}>
            <h2 id="simple-modal-title">Nueva Planta</h2>

            <form className={classes.root} noValidate autoComplete="off">
                <div>
                    <TextField
                        id="standard-password-input"
                        type="input"
                        value={nombre}
                        onChange={(event)=>setNombre(event.target.value)}
                        label="Nombre"
                        required
                        fullWidth
                        variant="outlined"
                        autoComplete="current-password" 
                    />
                    <TextField
                        id="standard-select-currency"
                        select
                        fullWidth
                        required
                        variant="outlined"
                        label="Tipo de planta"
                        value={subtipoId}
                        onChange={(event)=>setSubtipo(event.target.value)}
                        helperText="Elegí una opción"
                    >
                        {subtipos.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.nombre}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        id="standard-password-input"
                        type="input"
                        label="Descripción"
                        value={descripcion}
                        onChange={(event)=>setDescripcion(event.target.value)}
                        fullWidth
                        required
                        variant="outlined"
                    />
                    <TextField
                        id="standard-password-input"
                        type="input"
                        value={detalles}
                        onChange={(event)=>setDetalles(event.target.value)}
                        required
                        label="Detalles"
                        fullWidth
                        variant="outlined"
                    />
                    <TextField
                        id="standard-password-input"
                        type="input"
                        value={imagen}
                        onChange={(event)=>setImagen(event.target.value)}
                        required
                        label="Imagen"
                        fullWidth
                        variant="outlined"
                    />
                    <FormControlLabel 
                        label="Pet Friendly"
                        control={
                        <Checkbox
                            checked={petFriendly}
                            onChange={(event)=>setPetFriendly(event.target.checked)}
                            color="primary"
                            fullWidth
                            name="checkedA"
                        />}   
                    />                   
                    <div className={classes.button}>
                    <Button variant="contained" color="primary" onClick={guardar}>
                        Listo
                    </Button>
                    </div>
                </div>
            </form>
        </div>
    );

    return (
        <div>
            <Modal
                open={props.estaAbierto}
                onClose={props.handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                {body}
            </Modal>
        </div>
    )
}